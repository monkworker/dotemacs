;;;; package --- Sumary:
;;; Commentary:
;;
;; Autor: monkworker
;; Initial Commit: 07/03/2014
;; Last update: 30/03/2017
;;
;; The objective of this ".emacs" file is to fit my taste in
;; some programming languages.
;;
;; Currently it is, more or less, adapted for Python, Lisp,
;; Clojure, Javacript and C.
;;
;; The file was planned to download and install all dependencies
;; automaticaly, without the need of manual download of packages.
;; All it is needed is a internet connection.
;;
;; However, somethings must be installed manually:
;; - Inconsolota (system font).  A substitution would be DejaVu Sans Mono
;;
;; On the first use, an Emacs restart will be necessary.
;;
;; Many of the used code was based on the work of other people.
;; I just put it together.  I think others can look at this code, use it and
;; learn from it, the same way I have learned.
;;
;; Most part of this code was found in these blogs:
;;
;; http://www.wisdomandwonder.com/wordpress/wp-content/uploads/2014/03/C3F.html
;; http://www.kurup.org/blog/2012/10/24/emacs-for-python-programming/
;; http://www.jesshamrick.com/2012/09/18/emacs-as-a-python-ide/
;; http://caisah.info/emacs-for-python/
;; http://emacs-fu.blogspot.com.br
;; http://redbrain.co.uk/2013/11/11/my-dot-emacs-and-screenshot/
;;
;;
;;; Code:

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
  
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(prefer-coding-system 'utf-8)

;;------------------------------------------------------------------------------
;; LOAD PATH
;;------------------------------------------------------------------------------

;; Add DIR to load path
(add-to-list 'load-path "~/.emacs.d/lisp/")

;; add Custom themes load path
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")


;;------------------------------------------------------------------------------
;; PACKAGES (El-get, Elpa, Marmalade and Melpa)
;;------------------------------------------------------------------------------

;;package archive
(require 'package)
(package-initialize)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))

;;el-get
;;https://github.com/dimitri/el-get
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")
(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (let (el-get-master-branch)
      (goto-char (point-max))
      (eval-print-last-sexp))))
(el-get 'sync)


(require 'cl-lib)
;; Guarantee all packages are installed on start
(defvar packages-list
  '(python-django
    dired+
    ido
    ido-ubiquitous
    smex
    company
    jedi
    flycheck
    autopair
    rainbow-delimiters
    yasnippet
    fuzzy
    ;;virtualenvwrapper
    jdee
    kivy-mode
    js2-mode
    ac-js2
    highlight-numbers
    highlight-quoted
    highlight-symbol
    ;csharp-mode
    ;omnisharp
    clojure-mode
    cider
    smart-mode-line

    ;; themes
    ;color-theme-sanityinc-tomorrow
    ;zenburn-theme
    ;color-theme-solarized
    ;solarized-theme
    ;tangotango-theme
    ;ample-theme
    ;sublime-themes
    ;tao-theme
    flatland-theme)
  "List of packages needs to be installed at launch.")

(defun has-package-not-installed ()
  "Install the packages listed in 'packages-list'."
  (loop for p in packages-list
        when (not (package-installed-p p)) do (return t)
        finally (return nil)))
(when (has-package-not-installed)
  ;; Check for new packages (package versions)
  (message "%s" "Get latest versions of all packages...")
  (package-refresh-contents)
  (message "%s" " done.")
  ;; Install the missing packages
  (dolist (p packages-list)
    (when (not (package-installed-p p))
      (package-install p))))


;;------------------------------------------------------------------------------
;; USER EXPERIENCE
;;------------------------------------------------------------------------------

;; Smart-Mode-Line config. (improved "status line")
(setq sml/no-confirm-load-theme t)
(setq sml/theme 'dark)
(sml/setup)

;; Set Emacs to start fullscreen / fonte: http://caisah.info/emacs-for-python/
;; Work if it's running on GUI
(defun toggle-fullscreen ()
  "Open Emacs in fullscreen."
  (interactive)
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                         '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                         '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0)))

;; If running in GUI, go fullscreen
(if (display-graphic-p)
    (toggle-fullscreen))

;; set font (emacs wiki instruction)
;;(add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-10"))
(add-to-list 'default-frame-alist '(font . "Inconsolata-11"))
;;(set-frame-attribute 'default t :font "DejaVu Sans Mono-10")

;; Window configuration: disable menu-bar, tool-bar and scrollbar
(scroll-bar-mode -1)
(menu-bar-mode -1)
(tool-bar-mode -1)

;; Enable syntax highlighting when editing code.
(global-font-lock-mode 1)

;; highlight the current line
(global-hl-line-mode 1)

;; highlight parentheses when the cursor is next to them
(require 'paren)
(show-paren-mode t)
(setq blink-matching-paren nil)
(setq show-paren-delay 0)
(setq show-paren-style 'expression)

;; Install Autopair / fonte: http://caisah.info/emacs-for-python/
(require 'autopair)
(autopair-global-mode) ;; to enable in all buffers

;; turn on mouse wheel support for scrolling
(require 'mwheel)
(mouse-wheel-mode t)

;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under 
(setq scroll-step 1) ;; keyboard scroll one line at a time

;; Enable ido-mode
(require 'ido)
(ido-mode t)
(ido-everywhere 1)

;; ido in the M-x mini-buffer
(smex-initialize)
;; smex key binding
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;; ido-ubiquitous
(require 'ido-ubiquitous)
(ido-ubiquitous-mode 1)

;; Set Emacs to save buffers on exit
;; when start, reload buffers
(require 'desktop)
  (desktop-save-mode 1)
  (defun my-desktop-save ()
    (interactive)
    ;; Don't call desktop-save-in-desktop-dir, as it prints a message.
    (if (eq (desktop-owner) (emacs-pid))
        (desktop-save desktop-dirname)))
  (add-hook 'auto-save-hook 'my-desktop-save)

;; put all backup files on temp directory
(setq backup-directory-alist
      `((".*" . ,"~/.emacs-saves")))
(setq auto-save-file-name-transforms
      `((".*" , "~/.emacs-saves" t)))


;; backup files with versions
(setq version-control t     ;; Use version numbers for backups.
      kept-new-versions 1   ;; Number of newest versions to keep.
      kept-old-versions 0   ;; Number of oldest versions to keep.
      delete-old-versions t ;; Don't ask to delete excess backup versions.
      backup-by-copying t)  ;; Copy all files, don't rename them.

(setq vc-make-backup-files t)

;; date format for diary: DD/MM/YYYY
(setq european-calendar-style 't)

;;------------------------------------------------------------------------------
;; THEMES
;;------------------------------------------------------------------------------

;; load themes
;; All this themes are auto-installed.
;; Comment or uncomment to use.
;;
;; fogus-theme from sublime-themes
;;(load-theme 'fogus t)
;;---
;; zenburn
;;(load-theme 'zenburn t)
;;---
;; granger
;;(load-theme 'brin t)
;;---
;; tango-theme
;;(load-theme 'tangotango t)
;;---
;; charcola-black theme
;;(load-theme 'charcoal-black t)
;;---
;; ample-theme
;(load-theme 'ample t t)
;(enable-theme 'ample)
;;---
;; solarized-theme  ex:.(load-theme 'solarized-[light|dark] t)
;;(load-theme 'solarized t)
;;(set-frame-parameter nil 'background-mode 'dark)
;;(enable-theme 'solarized)
;;---
;; tao theme
;;(load-theme 'tao-yin t)
;;---
;; flatland theme
(load-theme 'flatland t)
;;---
;M-x color-theme-sanityinc-tomorrow-day
;M-x color-theme-sanityinc-tomorrow-night
;M-x color-theme-sanityinc-tomorrow-blue
;M-x color-theme-sanityinc-tomorrow-bright
;M-x color-theme-sanityinc-tomorrow-eighties


;;------------------------------------------------------------------------------
;; DIRED
;;------------------------------------------------------------------------------

;; loads dired+
(require 'dired+)
;;  Additional suggested key bindings (for dired+):
;;(define-key ctl-x-map   "d" 'diredp-dired-files)
;;(define-key ctl-x-4-map "d" 'diredp-dired-files-other-window)

;;------------------------------------------------------------------------------
;; CEDET
;;------------------------------------------------------------------------------

;; CEDET semantics config
(global-ede-mode 1)
(require 'semantic/sb)
(semantic-mode 1)

;-------------------------------------------------------------------------------
; GENERAL PROGRAMMING
;-------------------------------------------------------------------------------

;; text decoration
(require 'font-lock)
(setq font-lock-maximum-decoration t)
(global-font-lock-mode t)
(global-hi-lock-mode nil)
(setq jit-lock-contextually t)
(setq jit-lock-stealth-verbose t)

;; Highlight numbres with font-lock-constant-face
(add-hook 'prog-mode-hook 'highlight-numbers-mode) 

;; Highlight quoted
(add-hook 'prog-mode-hook 'highlight-quoted-mode) 

;; Highlight symbol
(add-hook 'prog-mode-hook 'highlight-symbol-mode) 

;; Rainbow delimiters: helps to see openning and closing paren
(require 'rainbow-delimiters)
;; Use raiwbow delimiters in all programming modes
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

; yas in all modes
(yas-global-mode 1)

;; each line of text gets one line on the screen (i.e., text will run
;; off the left instead of wrapping around onto a new line)
(set-default 'truncate-lines t)
; truncate lines even in partial-width windows
(setq truncate-partial-width-windows t)

;; highlight FIXME, TODO and BUG words
(defun warning-face ()
  "Set warning-face to words fixme, todo, and bug."
  (font-lock-add-keywords nil
    '(("\\<\\(FIXME\\|TODO\\|BUG\\):" 1 font-lock-warning-face t))))

(add-hook 'prog-mode-hook 'warning-face)

;; to setup tabs
(setq c-basic-indent 2)
(setq tab-width 4)

;; always use spaces, not tabs, when indenting
(setq-default indent-tabs-mode nil)

;; Company-mode
(add-hook 'after-init-hook 'global-company-mode)

;;------------------------------------------------------------------------------
;; C
;;------------------------------------------------------------------------------
;; set identation to 4 spaces
(setq-default c-basic-offset 4)

;;------------------------------------------------------------------------------
;; C# - .NET core
;;------------------------------------------------------------------------------
;(setq omnisharp-server-executable-path "~/omnisharp-roslyn/OmniSharp")
;(add-hook 'csharp-mode-hook 'omnisharp-mode)


;;------------------------------------------------------------------------------
;; PYTHON
;;------------------------------------------------------------------------------

;; loads python-django mode
;;(add-to-list 'load-path "~/.emacs.d/python-django/")
(require 'python-django)
(global-set-key (kbd "C-x j") 'python-django-open-project)

;; settings for Kivy Mode
(require 'kivy-mode)
(add-to-list 'auto-mode-alist '("\\.kv$" . kivy-mode))
;; Unlike python-mode, this mode follows the Emacs convention of not
;; binding the ENTER key to `newline-and-indent'. To get this behavior, add
;; the key definition to `kivy-mode-hook':

(add-hook 'kivy-mode-hook
      '(lambda ()
            (define-key kivy-mode-map "\C-m" 'newline-and-indent)))

;; initialize virtuaenvwrapper
;(require 'virtualenvwrapper)
;(venv-initialize-interactive-shells) ;; if you want interactive shell support
;(venv-initialize-eshell) ;; if you want eshell support
;(setq venv-location "~/.virtualenvs/")

;; Install Jedi.el / fonte: http://caisah.info/emacs-for-python/
;;(add-hook 'python-mode-hook 'auto-complete-mode)
;;(add-hook 'python-mode-hook 'jedi:ac-setup)
;(add-hook 'python-mode-hook 'jedi:setup)
;(setq jedi:complete-on-dot t)   

;; Install Flycheck / fonte: http://caisah.info/emacs-for-python/
(add-hook 'after-init-hook #'global-flycheck-mode)

;;------------------------------------------------------------------------------
;; HTML
;;------------------------------------------------------------------------------

;; Identation for HTML and SGML mode
(setq sgml-basic-offset 4)


;;------------------------------------------------------------------------------
;; JAVASCRIPT
;;------------------------------------------------------------------------------

;; Javascript-mode
(add-hook 'js-mode-hook 'js2-minor-mode)
(add-hook 'js2-mode-hook 'ac-js2-mode)
(setq js2-highlight-level 3)

;; Indentation for JavaScript Mode
(setq js-indent-level 4)

;; Identation with tabs
(setq js2-mode-hook
  '(lambda () (progn
    (set-variable 'indent-tabs-mode nil))))

;;------------------------------------------------------------------------------
;; CLOJURE
;;------------------------------------------------------------------------------

;; CIDER config (Clojure)
(require 'cider)
;;(add-to-list 'auto-minor-mode-alist '("\\.clj$" . cider-mode))

;; Use cider-mode as well
(add-hook 'clojure-mode-hook 'cider-mode)

;;; Use company-mode on Cider
(add-hook 'cider-repl-mode-hook #'company-mode)
(add-hook 'cider-mode-hook #'company-mode)

;; Enable eldoc in Clojure buffers:
(add-hook 'cider-mode-hook #'eldoc-mode)

(add-hook 'clojure-mode-hook #'rainbow-delimiters-mode)

;; Log communication with the nREPL server (extremely useful for debugging CIDER problems)
;(setq nrepl-log-messages t)

;; Error messages may be wrapped for readability.
(setq cider-stacktrace-fill-column 80)

;; Set Speclj indentaion
(put 'describe 'clojure-backtracking-indent '(4 2))
(put 'it 'clojure-backtracking-indent '(4 2))
(put 'before 'clojure-backtracking-indent '(2))
(put 'before-all 'clojure-backtracking-indent '(2))
(put 'after-all 'clojure-backtracking-indent '(2))
(put 'after 'clojure-backtracking-indent '(2))
(put 'with 'clojure-backtracking-indent '(2))

;;------------------------------------------------------------------------------
;; LISP
;;------------------------------------------------------------------------------

;; highlight quoted text on lisp-mode
(add-hook 'lisp-mode-hook 'highlight-quoted-mode)

(require 'highlight-symbol)
(global-set-key [(control f3)] 'highlight-symbol)
(global-set-key [f3] 'highlight-symbol-next)
(global-set-key [(shift f3)] 'highlight-symbol-prev)
(global-set-key [(meta f3)] 'highlight-symbol-query-replace)

;; if there is size information associated with text, change the text
;; size to reflect it
(size-indication-mode t)

(defun byte-compile-current-buffer ()
  "'byte-compile' current buffer if it's 'emacs-lisp-mode' and compiled file exists."
  (interactive)
  (when (and (eq major-mode 'emacs-lisp-mode)
             (file-exists-p (byte-compile-dest-file buffer-file-name)))
    (byte-compile-file buffer-file-name)))
(add-hook 'after-save-hook 'byte-compile-current-buffer)

;;------------------------------------------------------------------------------
;; JAVA
;;------------------------------------------------------------------------------
(setq auto-mode-alist
      (append '(("\\.java\\'" . jdee-mode)) auto-mode-alist))

;;------------------------------------------------------------------------------
;; SHELL
;;------------------------------------------------------------------------------

;; initialize tramp for ssh conection

(require 'tramp)
(setq tramp-default-method "ssh")

;; settings to not display passwords on shell
;; 'learning GNU Emacs' book
(add-hook 'comint-output-filter-functions
          'comint-watch-for-password-prompt)

;;------------------------------------------------------------------------------
;; ORG-MODE
;;------------------------------------------------------------------------------

(require 'org)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))

;;; .emacs ends here
