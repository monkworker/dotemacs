# README #

The objective of this ".emacs" file is to fit my taste in some programming languages. 
Also, I tried to construct a configuration that can download all dependencies itself. 

This '.emacs' file was designed for **GNU Emacs 24.3**

I have been using it in GNU Emacs 24.5.1 too. Ubuntu 14.4 / Debian Wheezy.

## Requirements ##

* GNU Emacs 24.3 (not tested on version 25);
* Inconsolota font; (DejaVu Sans Mono can be used instead, with some minor modification)

## Features ##

Currently, the focus is on these programming languagues:

* Python/Django;
* Lisp;
* Clojure;
* HTML; 
* Javascript;
* C.

## Dependencies ##

The file was planned to download and install all dependencies automaticaly, without the need of manual packages download. 

When used for the first time, an Emacs restart must be necessary.

## Resources ##

Many of the used code was based on the work presented by the resources below (I just put it together), and I recommend that you read it.
I think others can look at this code, use it and learn from it, the same way I could learn something.

* EmacsWiki
* http://www.wisdomandwonder.com/wordpress/wp-content/uploads/2014/03/C3F.html
* http://www.kurup.org/blog/2012/10/24/emacs-for-python-programming/
* http://www.jesshamrick.com/2012/09/18/emacs-as-a-python-ide/
* http://caisah.info/emacs-for-python/
* http://emacs-fu.blogspot.com.br
* http://redbrain.co.uk/2013/11/11/my-dot-emacs-and-screenshot/